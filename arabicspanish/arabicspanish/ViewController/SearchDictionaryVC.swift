//
//  SearchDictionaryVC.swift
//  arabicspanish
//
//  Created by devsenior on 04/09/2021.
//

import UIKit

class SearchDictionaryVC: UIViewController,UISearchBarDelegate {
    @IBOutlet weak var collectionViewMain: UICollectionView!
    var DataItems:[ItemsModel] = SqliteService.shared.DataItems
    @IBOutlet weak var searchNextBar: UISearchBar!
    var DataSearchItems:[ItemsModel] = [ItemsModel]()
    var isSearchPro = false
    @IBAction func backAppPro(){
        self.dismiss(animated: true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        DataSearchItems.removeAll()
        if searchText.count == 0{
            isSearchPro = false
        }
        for item in DataItems{
            if item.en.contains(searchText) == true{
                DataSearchItems.append(item)
                isSearchPro = true
            }
        }
        collectionViewMain.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AdmobManager.shared.logEvent()
        self.view.addSubview(AdmobManager.shared.createBannerView(inVC: self))
        collectionViewMain.register(UINib(nibName: ItemCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: ItemCollectionViewCell.className)
        collectionViewMain.backgroundColor = UIColor.clear
        if DataItems.count == 0{
            SqliteService.shared.getDataItems(){data,error in
                if let data = data{
                    self.DataItems = data
                }
            }
        }
    }
}

extension SearchDictionaryVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int { //numberOfSections muốn bao nhiêu nhóm trong CollectionView
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) ->
    //numberOfItemsInSection muốn bao nhiêu dữ liệu
    Int {return DataSearchItems.count}
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) ->
    //cellForItemAt indexPath muốn hiển thị gì trong CollectionView
        UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemCollectionViewCell.className, for: indexPath) as!ItemCollectionViewCell
        cell.ViewNgoai.backgroundColor = UIColor(red: 206/255, green: 218/255, blue: 255/255, alpha: 1.0) //206, 218, 255
        cell.labelname.text =  self.DataSearchItems[indexPath.row].es
        cell.labeldes.text =  self.DataSearchItems[indexPath.row].de
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        return UICollectionReusableView()
    }
}

extension SearchDictionaryVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let anh = self.storyboard?.instantiateViewController(withIdentifier: "PlayViewcontroller") as! PlayViewcontroller
        anh.modalPresentationStyle = .fullScreen
        anh.data = self.DataSearchItems[indexPath.row].cat_name
        anh.dataes = self.DataSearchItems[indexPath.row].es
        anh.dataed = self.DataSearchItems[indexPath.row].de
        thissong = indexPath.row
        self.present(anh, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt
        indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: UIScreen.main.bounds.width/2 - 10, height: 180)
        }
        return CGSize(width: UIScreen.main.bounds.width - 10, height: 180)
    }
}

