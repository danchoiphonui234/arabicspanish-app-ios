//
//  FavoriteViewController.swift
//  arabicspanish
//
//  Created by devsenior on 16/07/2021.
//

import UIKit
import SQLite

class FavoriteViewController: UIViewController {
    @IBOutlet weak var collectionViewMain: UICollectionView!
    var DataItems:[ItemsModel] = [ItemsModel]()
    @IBAction func searchButtonAction(){
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchDictionaryVC") as! SearchDictionaryVC
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        AdmobManager.shared.logEvent()
        self.view.addSubview(AdmobManager.shared.createBannerView(inVC: self))
        collectionViewMain.backgroundColor = UIColor.clear
        SqliteService.shared.getDataItems(){data,error in
            if let data = data{
                for item in data{
                    if item.fav != "Null"{
                        self.DataItems.append(item)
                    }
                }
                self.collectionViewMain.reloadData()
            }
        }
        collectionViewMain.register(UINib(nibName: ItemCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: ItemCollectionViewCell.className)

    }
    
    @IBAction func Back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
extension FavoriteViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int { //numberOfSections muốn bao nhiêu nhóm trong CollectionView
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) ->
    //numberOfItemsInSection muốn bao nhiêu dữ liệu
    Int {return DataItems.count}
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) ->
    //cellForItemAt indexPath muốn hiển thị gì trong CollectionView
        UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemCollectionViewCell.className, for: indexPath) as!ItemCollectionViewCell
        cell.ViewNgoai.backgroundColor = UIColor(red: 206/255, green: 218/255, blue: 255/255, alpha: 1.0) //206, 218, 255
        cell.labelname.text =  self.DataItems[indexPath.row].es
        cell.labeldes.text =  self.DataItems[indexPath.row].de
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        return UICollectionReusableView()
    }
    
}

extension FavoriteViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let anh = self.storyboard?.instantiateViewController(withIdentifier: "PlayViewcontroller") as! PlayViewcontroller
        anh.modalPresentationStyle = .fullScreen
        anh.data = self.DataItems[indexPath.row].cat_name
        anh.dataes = self.DataItems[indexPath.row].es
        anh.dataed = self.DataItems[indexPath.row].de
        thissong = indexPath.row
        self.present(anh, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt
        indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: UIScreen.main.bounds.width/2 - 10, height: 180)
        }
        return CGSize(width: UIScreen.main.bounds.width - 10, height: 180)
    }
}

