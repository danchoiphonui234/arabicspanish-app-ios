//
//  PlayViewcontroller.swift
//  arabicspanish
//
//  Created by devsenior on 14/07/2021.
//
import AVFoundation
import UIKit


class PlayViewcontroller: UIViewController,UICollectionViewDelegate {
    var DataItems:[ItemsModel] = [ItemsModel]()
    var indexNext = 0
    var data:String = ""
    var dataes: String = ""
    var dataed: String = ""
    var UrlImage:String = ""
    
    
    @IBOutlet weak var Labelname: UILabel!
    @IBOutlet weak var buttonFav: UIButton!
    
    @IBOutlet weak var Viewngoai: UIView!
    @IBOutlet weak var ViewTrong: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var Labeles: UILabel!
    @IBOutlet weak var Labelde: UILabel!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // unfavorite
        if DataItems[indexNext].fav !=  "Null"{
            let btnImage = UIImage(named: "Favirote")
            buttonFav.setImage(btnImage , for: .normal)
        }else{
            let btnImage = UIImage(named: "unfavorite")
            buttonFav.setImage(btnImage , for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Labelname.text = data
        AdmobManager.shared.logEvent()
        self.view.addSubview(AdmobManager.shared.createBannerView(inVC: self))
        Labeles.text = dataes
        Labelde.text = dataed
        if UrlImage.count > 0{
            let start = UrlImage.index(UrlImage.startIndex, offsetBy: 9)
            let range = start..<UrlImage.endIndex
            let mySubstring = UrlImage[range]
            img.image = UIImage.init(named: String(mySubstring))
            Viewngoai.layer.cornerRadius = 30
            ViewTrong.layer.cornerRadius = 5
            img.layer.cornerRadius = 40
        }
        
        
        SqliteService.shared.getDataItems(){data,error in
            if let data = data{
                self.DataItems = data
            }
            
        }
        
    }
    
    
    @IBAction func BtnHeart(_ sender: UIButton) {
        //addFavoriteDatabase
        
        if  DataItems[indexNext].fav.count == 0 || DataItems[indexNext].fav == "Null" {
            SqliteService.shared.addFavoriteDatabase(id: DataItems[indexNext]._id,favorite: "True")
            let btnImage = UIImage(named: "Favirote")
            buttonFav.setImage(btnImage , for: .normal)
        }else{
            SqliteService.shared.addFavoriteDatabase(id: DataItems[indexNext]._id,favorite: "Null")
            let btnImage = UIImage(named: "unfavorite")
            buttonFav.setImage(btnImage , for: .normal)
        }
        
    }
    
    
    @IBAction func BtnCopy(_ sender: Any) {
        UIPasteboard.general.string = self.DataItems[indexNext].en + " - " + self.DataItems[indexNext].es + " - " + self.DataItems[indexNext].de
    }
    
    @IBAction func BackPlay(_ sender: UIButton) {
        if indexNext - 1 > 0{
            indexNext = indexNext - 1
            Labelname.text = self.DataItems[indexNext].en
            Labeles.text = self.DataItems[indexNext].es
            Labelde.text = self.DataItems[indexNext].de
        }
    }
    @IBAction func Play(_ sender: Any) {
        Labeles.resignFirstResponder()
        SpeechService.shared.startSpeech(Labeles.text!)
    }
    
    @IBAction func NextPlay(_ sender: Any) {
        if self.DataItems.count - 1 > indexNext{
            indexNext = indexNext + 1
            Labelname.text = self.DataItems[indexNext].en
            Labeles.text = self.DataItems[indexNext].es
            Labelde.text = self.DataItems[indexNext].de
        }
        
    }
    
    
    @IBAction func Share(_ sender: Any) {
        let text = "Share in this app: " + self.DataItems[indexNext].en + " - " + self.DataItems[indexNext].es

        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = sender as! UIView // so that iPads won't crash

        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]

        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func BTnback(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
}
