//
//  MainSettings.swift
//  arabicspanish
//
//  Created by devsenior on 14/07/2021.
//

import UIKit

class MainSettings: UIViewController {

    @IBOutlet weak var Viewduoi: UIView!
    var listdata:[ItemsModel] = [ItemsModel]()
    
    var index = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        Viewduoi.layer.cornerRadius = 30
        listdata = SqliteService.shared.DataItems
        AdmobManager.shared.logEvent()
        self.view.addSubview(AdmobManager.shared.createBannerView(inVC: self))
        if listdata.count == 0{
            SqliteService.shared.getDataItems(){data,error in
                if let data = data{
                    self.listdata = data
                }
            }
        }
    }
    
    @IBAction func Category1(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func Category2(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Favorite1(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FavoriteViewController") as! FavoriteViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Favorite2(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FavoriteViewController") as! FavoriteViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func loadAboutPro(_ sender: Any) {
        let anh = self.storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
        anh.modalPresentationStyle = .fullScreen
        self.present(anh, animated: true)
    }
    
    @IBAction func randomItem(_ sender: UIButton)   {
        
    }
    @IBAction func random(_ sender: Any) {
        let anh = self.storyboard?.instantiateViewController(withIdentifier: "PlayViewcontroller") as! PlayViewcontroller
        anh.modalPresentationStyle = .fullScreen
        let randomInt = Int.random(in: 0..<listdata.count - 1)
        anh.data = self.listdata[randomInt].cat_name
        anh.dataes = self.listdata[randomInt].es
        anh.dataed = self.listdata[randomInt].de
        thissong = randomInt
        self.present(anh, animated: true, completion: nil)
    }
    ////////////////////////
    
    @IBAction func notificacion(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PolicyVC") as! PolicyVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func notificacion1(_ sender: Any) {
    }
    
    @IBAction func BtnBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
