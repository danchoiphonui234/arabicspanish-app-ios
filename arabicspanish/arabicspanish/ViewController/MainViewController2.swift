//
//  MainViewController2.swift
//  arabicspanish
//
//  Created by devsenior on 08/07/2021.
//

import UIKit

class MainViewController2: UIViewController {

    
    @IBOutlet weak var collectionView: UICollectionView!
    var DataCategory:[CategoryModel] = [CategoryModel]()
    var indexNext = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: MainHomeCLVCell.className, bundle: nil), forCellWithReuseIdentifier: MainHomeCLVCell.className)
        SqliteService.shared.getDataCategory(){data,error in
            if let data = data{
                self.DataCategory = data
            }
        }
    }

    
}

extension MainViewController2: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MainHomeCLVCell.className, for: indexPath) as! MainHomeCLVCell
        cell.viewNgoai.backgroundColor = UIColor(red: 206/255, green: 218/255, blue: 255/255, alpha: 1.0) //206, 218, 255
        cell.LabelName.text =  self.DataCategory[indexPath.row + indexNext * 4].cen
        let stringPro = self.DataCategory[indexPath.row + indexNext * 4].img
        let start = stringPro.index(stringPro.startIndex, offsetBy: 9)
        let range = start..<stringPro.endIndex
        let mySubstring = stringPro[range]
        cell.imageCover.image = UIImage.init(named: String(mySubstring))
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        return UICollectionReusableView()
    }
    
}

extension MainViewController2: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: UIScreen.main.bounds.width/4 - 30, height: 150)
        }
        return CGSize(width: UIScreen.main.bounds.width/2 - 30, height: 150)
    }

}
