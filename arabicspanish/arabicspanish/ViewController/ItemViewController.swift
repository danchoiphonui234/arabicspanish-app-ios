//
//  ItemViewController.swift
//  arabicspanish
//
//  Created by devsenior on 09/07/2021.
//

import UIKit
import AVFoundation


var thissong = 0

class ItemViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource{
    var data:String = ""
    var idSend:String = ""
    var UrlImage:String = ""
    @IBOutlet weak var textViewMain: UITextView!
    @IBOutlet weak var labelname: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var DataItems:[ItemsModel] = [ItemsModel]()
    var DataCategory:[CategoryModel] = [CategoryModel]()
    var indexNext = 0
    
    @IBAction func searchAppPro(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchDictionaryVC") as! SearchDictionaryVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundColor = UIColor.clear
        labelname.text = data
        AdmobManager.shared.logEvent()
        self.view.addSubview(AdmobManager.shared.createBannerView(inVC: self))
        collectionView.register(UINib(nibName: ItemCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: ItemCollectionViewCell.className)
        SqliteService.shared.getDataItems(){data,error in
            if let data = data{
                for item1 in data{
                    if item1.id_cat == self.idSend{
                        self.DataItems.append(item1)
                    }
                }
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataItems.count
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int { //numberOfSections muốn bao nhiêu nhóm trong CollectionView
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) ->
    //cellForItemAt indexPath muốn hiển thị gì trong CollectionView
    UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemCollectionViewCell.className, for: indexPath) as!ItemCollectionViewCell
        cell.ViewNgoai.backgroundColor = UIColor(red: 206/255, green: 218/255, blue: 255/255, alpha: 1.0) //206, 218, 255
        cell.labelname.text =  self.DataItems[indexPath.row + indexNext * 4].es
        cell.labeldes.text =  self.DataItems[indexPath.row + indexNext * 4].de
        cell.backgroundColor = UIColor.clear
        cell.layer.cornerRadius = 15
        cell.layer.masksToBounds = true
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        return UICollectionReusableView()
    }
    
    
    
}
extension ItemViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: UIScreen.main.bounds.width, height: 150)
        }
        return CGSize(width: UIScreen.main.bounds.width, height: 150)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let anh = self.storyboard?.instantiateViewController(withIdentifier: "PlayViewcontroller") as! PlayViewcontroller
        anh.modalPresentationStyle = .fullScreen
        anh.data = self.DataItems[indexPath.row + indexNext * 4].cat_name
        anh.dataes = self.DataItems[indexPath.row + indexNext * 4].es
        anh.dataed = self.DataItems[indexPath.row + indexNext * 4].de
        anh.UrlImage = self.UrlImage
        thissong = indexPath.row
        
        self.present(anh, animated: true, completion: nil)
    }
    
    
    @IBAction func Btnback(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
}

