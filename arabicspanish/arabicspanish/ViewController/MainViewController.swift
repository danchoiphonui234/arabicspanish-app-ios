//
//  MainViewController.swift
//  arabicspanish
//
//  Created by devsenior on 05/07/2021.
//

import UIKit
import AppTrackingTransparency

class MainViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    var DataCategory:[CategoryModel] = [CategoryModel]()
    var indexNext = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        AdmobManager.shared.fullRootViewController = self
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
                AdmobManager.shared.createAndLoadInterstitial()
            })
        } else {
            AdmobManager.shared.createAndLoadInterstitial()
        }
        AdmobManager.shared.logEvent()
        self.view.addSubview(AdmobManager.shared.createBannerView(inVC: self))
        collectionView.backgroundColor = UIColor.clear
        collectionView.register(UINib(nibName: MainHomeCLVCell.className, bundle: nil), forCellWithReuseIdentifier: MainHomeCLVCell.className)
        SqliteService.shared.getDataCategory(){data,error in
            if let data = data{
                self.DataCategory = data
            }
        }
        
    }
    @IBAction func BtnSettings(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainSettings") as! MainSettings
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func NextAction(_ sender: Any) {
        indexNext = indexNext + 1
        if self.DataCategory.count > indexNext * 4 {
            self.collectionView.scrollToItem(at:IndexPath(item: (indexNext) * 4, section: 0), at: .right, animated: false)
            self.collectionView.reloadData()
        }
    }
    @IBAction func PreviewAction(_ sender: Any) {
        indexNext = indexNext - 1
        self.collectionView.scrollToItem(at:IndexPath(item: (indexNext) * 4, section: 0), at: .right, animated: false)
        self.collectionView.reloadData()
    }
    
    @IBAction func searchActionpro(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchDictionaryVC") as! SearchDictionaryVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int { //numberOfSections muốn bao nhiêu nhóm trong CollectionView
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    //numberOfItemsInSection muốn bao nhiêu dữ liệu
        return DataCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) ->
    //cellForItemAt indexPath muốn hiển thị gì trong CollectionView
    UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MainHomeCLVCell.className, for: indexPath) as!MainHomeCLVCell
        cell.viewNgoai.backgroundColor = UIColor(red: 206/255, green: 218/255, blue: 255/255, alpha: 1.0) //206, 218, 255
        if self.DataCategory.count > indexPath.row{
            let stringPro = self.DataCategory[indexPath.row ].img
            cell.LabelName.text =  self.DataCategory[indexPath.row ].cen
            cell.LabelDes.text =  self.DataCategory[indexPath.row].ces
            let start = stringPro.index(stringPro.startIndex, offsetBy: 9)
            let range = start..<stringPro.endIndex
            let mySubstring = stringPro[range]
            cell.imageCover.image = UIImage.init(named: String(mySubstring))
            cell.viewNgoai.backgroundColor = UIColor{_ in return UIColor(red: .random(in: 0...1), green: .random(in: 0...1), blue: .random(in: 0...1), alpha: 1)}
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        
        return UICollectionReusableView()
    }
    
}

extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //didSelectItemAt indexPath click item
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ItemViewController") as! ItemViewController
        vc.modalPresentationStyle = .fullScreen
        vc.data = self.DataCategory[indexPath.row].cen
        vc.idSend = self.DataCategory[indexPath.row]._id
        vc.UrlImage = self.DataCategory[indexPath.row].img
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt
                            indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: UIScreen.main.bounds.width/2 - 2, height: 230)
        }
        return CGSize(width: UIScreen.main.bounds.width/2 - 2, height: 250)
    }
}

