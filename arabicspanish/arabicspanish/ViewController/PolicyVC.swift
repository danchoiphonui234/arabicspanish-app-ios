//
//  PolicyVC.swift
//  arabicspanish
//
//  Created by devsenior on 04/09/2021.
//

import UIKit

class PolicyVC: UIViewController {
    @IBOutlet weak var textViewMain:UITextView!
    @IBAction func backAppPro(){
        self.dismiss(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        textViewMain.backgroundColor = UIColor.clear
        textViewMain.isEditable = false
        AdmobManager.shared.logEvent()
        self.view.addSubview(AdmobManager.shared.createBannerView(inVC: self))
    }
    

}
