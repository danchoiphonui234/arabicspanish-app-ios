//
//  AboutViewController.swift
//  arabicspanish
//
//  Created by devsenior on 04/09/2021.
//

import UIKit

class AboutViewController: UIViewController {
    @IBOutlet weak var textViewMain:UITextView!
    @IBAction func backAppPro(){
        self.dismiss(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        AdmobManager.shared.logEvent()
        self.view.addSubview(AdmobManager.shared.createBannerView(inVC: self))
        textViewMain.backgroundColor = UIColor.clear
        textViewMain.isEditable = false
    }

}
