//
//  MainHomeCLVCell.swift
//  arabicspanish
//
//  Created by devsenior on 08/07/2021.
//

import UIKit
class MainHomeCLVCell: UICollectionViewCell {
    @IBOutlet weak var LabelDes:UILabel!
    @IBOutlet weak var LabelName:UILabel!
    @IBOutlet weak var imageCover:UIImageView!
    @IBOutlet weak var viewNgoai:UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewNgoai.layer.cornerRadius = 20
        imageCover.layer.cornerRadius = 40
        
    }
    
    
}

