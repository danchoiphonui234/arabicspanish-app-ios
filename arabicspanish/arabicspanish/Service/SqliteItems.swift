//
//  SqliteItems.swift
//  arabicspanish
//
//  Created by devsenior on 11/07/2021.
//

import UIKit
import SQLite

class SqliteItems: NSObject {
    static let shared: SqliteItems = SqliteItems()
    var DatabaseRoot:Connection?
    var DataItems:[ItemsModel] = [ItemsModel]()
    func loadInit(linkPath:String){_ = Bundle.main.bundlePath
        do {
            self.DatabaseRoot = try Connection (linkPath)
        } catch {
            print(error)
        }
    }
    
    func getDataItems(closure: @escaping (_ response: [ItemsModel]?, _ error: Error?) -> Void) {
        let users1 = Table("Items")
        let id1 = Expression<String>("_id")
        let id_cat1 = Expression<String?>("id_cat")
        let cat_name1 = Expression<String>("cat_name")
        let en1 = Expression<String>("en")
        let fr1 = Expression<String>("fr")
        let es1 = Expression<String>("es")
        let de1 = Expression<String>("de")
        let it1 = Expression<String>("it")
        let tur1 = Expression<String>("tur")
        let rus1 = Expression<String>("rus")
        let ar1 = Expression<String>("ar")
        let far1 = Expression<String>("far")
        
        
        DataItems.removeAll()
        if let DatabaseRoot = DatabaseRoot{
            do{
                for user in try DatabaseRoot.prepare(users1) {
                    DataItems.append(ItemsModel(_id: user[id1]
                                                , id_cat: user[id_cat1] ?? ""
                                                      , cat_name: user[cat_name1]
                                                      , en: user[en1]
                                                      , fr: user[fr1]
                                                      , es: user[es1]
                                                      , de: user[de1]
                                                      , it: user[it1]
                                                      , tur: user[tur1]
                                                      , rus: user[rus1]
                                                      , ar: user[ar1]
                                                , fav: user[far1]))
                    
                }
            } catch  {}
        }
        closure(DataItems, nil)
        
    }
    
}


