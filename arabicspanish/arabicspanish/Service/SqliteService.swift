//
//  SqliteService.swift
//  arabicspanish
//
//  Created by devsenior on 06/07/2021.
//

import UIKit
import SQLite

class SqliteService: NSObject {
    static let shared: SqliteService = SqliteService()
    var DatabaseRoot:Connection?
    var DataCategory:[CategoryModel] = [CategoryModel]()
    var DataItems:[ItemsModel] = [ItemsModel]()
    func loadInit(linkPath:String){_ = Bundle.main.bundlePath
        do {
            self.DatabaseRoot = try Connection (linkPath)
        } catch {
            print(error)
        }
    }
    
    func getDataCategory(closure: @escaping (_ response: [CategoryModel]?, _ error: Error?) -> Void) {
        let users1 = Table("Category")
        let id1 = Expression<String>("_id")
        let cen1 = Expression<String?>("cen")
        let cfr1 = Expression<String>("cfr")
        let ces1 = Expression<String>("ces")
        let cde1 = Expression<String>("cde")
        let clt1 = Expression<String>("cit")
        let ctur1 = Expression<String>("ctur")
        let crus1 = Expression<String>("crus")
        let car1 = Expression<String>("car")
        let img1 = Expression<String>("img")
        
        
        DataCategory.removeAll()
        if let DatabaseRoot = DatabaseRoot{
            do{
                for user in try DatabaseRoot.prepare(users1) {
                    DataCategory.append(CategoryModel(_id: user[id1]
                                                      , cen: user[cen1] ?? ""
                                                      , cfr: user[cfr1]
                                                      , ces: user[ces1]
                                                      , cde: user[cde1]
                                                      , clt: user[clt1]
                                                      , ctur: user[ctur1]
                                                      , crus: user[crus1]
                                                      , img: user[img1]
                                                      , car: user[car1]))
                }
            } catch  {}
        }
        closure(DataCategory, nil)
        
    }
    func getDataItems(closure: @escaping (_ response: [ItemsModel]?, _ error: Error?) -> Void) {
        let users1 = Table("Items")
        let id1 = Expression<String>("_id")
        let id_cat1 = Expression<String?>("id_cat")
        let cat_name1 = Expression<String>("cat_name")
        let en1 = Expression<String>("en")
        let fr1 = Expression<String>("fr")
        let es1 = Expression<String>("es")
        let de1 = Expression<String>("de")
        let it1 = Expression<String>("it")
        let tur1 = Expression<String>("tur")
        let rus1 = Expression<String>("rus")
        let ar1 = Expression<String>("ar")
        let far1 = Expression<String>("fav")
        
        
        DataItems.removeAll()
        if let DatabaseRoot = DatabaseRoot{
            do{
                for user in try DatabaseRoot.prepare(users1) {
                    DataItems.append(ItemsModel(_id: user[id1]
                                                , id_cat: user[id_cat1] ?? ""
                                                      , cat_name: user[cat_name1]
                                                      , en: user[en1]
                                                      , fr: user[fr1]
                                                      , es: user[es1]
                                                      , de: user[de1]
                                                      , it: user[it1]
                                                      , tur: user[tur1]
                                                      , rus: user[rus1]
                                                      , ar: user[ar1]
                                                , fav: user[far1]))
                    
                }
            } catch  {}
        }
        closure(DataItems, nil)
    }
    
    func addFavoriteDatabase(id:String, favorite:String) -> Void {
            let users = Table("Items")
            let fav = Expression<String>("fav")
            let id1 = Expression<String>("_id")
        
            if let DatabaseRoot = DatabaseRoot {
                do {
                    let alice = users.filter(id1 == id)
                    try DatabaseRoot.run(alice.update(fav <- favorite))
                } catch{
                    print(error)
                    return
                }
            }
            
        }
}
