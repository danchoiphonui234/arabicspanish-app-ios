//
//  SpeechService.swift
//  arabicspanish
//
//  Created by devsenior on 20/07/2021.
//

import UIKit
import AVKit

class SpeechService: NSObject {
   static let shared = SpeechService()
    
    let speechSynthesizer = AVSpeechSynthesizer()
    
    func startSpeech(_ text: String) {
        self.stopSpeeching()
        
        if let language = NSLinguisticTagger.dominantLanguage(for: text){
            let utterence = AVSpeechUtterance(string: text);utterence.voice = AVSpeechSynthesisVoice(language: language)
            
            
            speechSynthesizer.speak(utterence)
        }
    }
        
        func stopSpeeching(){
            speechSynthesizer.stopSpeaking(at: .immediate)
        }
        
    
}
