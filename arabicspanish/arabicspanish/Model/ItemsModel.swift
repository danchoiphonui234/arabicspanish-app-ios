//
//  ItemsModel.swift
//  arabicspanish
//
//  Created by devsenior on 07/07/2021.
//

import UIKit

class ItemsModel: NSObject {
    var _id : String = ""
    var id_cat : String = ""
    var cat_name : String = ""
    var en : String = ""
    var fr : String = ""
    var es : String = ""
    var de : String = ""
    var it : String = ""
    var tur : String = ""
    var rus : String = ""
    var ar : String = ""
    var fav : String = ""
    
    init(_id:String,id_cat:String,cat_name:String,en:String,fr:String,es:String,de:String,it:String,tur:String,rus:String,ar:String,fav:String){
        self._id = _id
        self.id_cat = id_cat
        self.cat_name = cat_name
        self.en = en
        self.fr = fr
        self.es = es
        self.de = de
        self.it = it
        self.tur = tur
        self.rus = rus
        self.ar = ar
        self.fav = fav
        
    }
}
