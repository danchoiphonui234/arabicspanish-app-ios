//
//  CategoryModel.swift
//  arabicspanish
//
//  Created by devsenior on 07/07/2021.
//

import UIKit


class CategoryModel: NSObject {
    var _id : String = ""
    var cen : String = ""
    var cfr : String = ""
    var ces : String = ""
    var cde : String = ""
    var clt : String = ""
    var ctur : String = ""
    var crus : String = ""
    var img : String = ""
    var car : String = ""
    var id_cat : String = ""

    init(_id:String,cen:String,cfr:String,ces:String,cde:String,clt:String,ctur:String,crus:String,img:String,car:String) {
        self._id = _id
        self.cen = cen
        self.cfr = cfr
        self.ces = ces
        self.cde = cde
        self.clt = clt
        self.ctur = ctur
        self.crus = crus
        self.img = img
        self.car = car
    }
}
